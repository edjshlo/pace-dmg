﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogManager : MonoBehaviour {

	public GameObject dialogPanel;
	public Text NPCName; 
	public Text dialogText; 

	private List<string> dialog; 
	private int dialogIndex; 

	// Use this for initialization
	void Start () {

		dialogPanel.SetActive (false); 
		
	}

	public void startDialog(string name, List<string> conversation) { 

		NPCName.text = name; 
		conversation = new List<string> (conversation); 
		dialogIndex = 0; 
		dialogPanel.SetActive (true); 
		showText (); 

	} 

	public void stopDialog() { 

		dialogPanel.SetActive (false); 

	}

	public void showText() { 

		dialogText.text = dialog [dialogIndex]; 

	} 

	public void Next() { 

		if(dialogIndex < dialog.Count -1) { 
			dialogIndex += 1; 
			showText (); 
		}

	}


}
