﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	public string name; 
	public DialogManager dialogManager; 
	public List<string> npcDialog = new List<string>(); 

	public void Next() { 

		dialogManager.startDialog (name, npcDialog); 

	}

}
